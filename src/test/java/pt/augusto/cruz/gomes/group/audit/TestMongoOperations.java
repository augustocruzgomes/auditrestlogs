package pt.augusto.cruz.gomes.group.audit;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.mongodb.Mongo;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import pt.augusto.cruz.gomes.group.audit.domain.Action;
import pt.augusto.cruz.gomes.group.audit.domain.User;
import pt.augusto.cruz.gomes.group.audit.mongo.repository.UserRepository;


//@RunWith(SpringRunner.class)
//@SpringBootTest
public class TestMongoOperations {
	
	private static final String LOCALHOST = "127.0.0.1";
    private static final String DB_NAME = "audits";
    private static final int MONGO_TEST_PORT = 27028;

//	@Autowired
//	private UserRepository userRepository;
//	private SampleRepositoryMongoImpl repoImpl;
    
    private UserRepository userRepository;

    private static MongodProcess mongoProcess;
    private static Mongo mongo;
    
    private MongoTemplate template;
	/*
    @BeforeClass
    public static void initializeDB() throws IOException {

        RuntimeConfig config = new RuntimeConfig();
        config.setExecutableNaming(new UserTempNaming());

        MongodStarter starter = MongodStarter.getInstance(config);

        MongodExecutable mongoExecutable = starter.prepare(new MongodConfig(Version.V2_2_0, MONGO_TEST_PORT, false));
        mongoProcess = mongoExecutable.start();

        mongo = new Mongo(LOCALHOST, MONGO_TEST_PORT);
        mongo.getDB(DB_NAME);
    }
	
	
    @AfterClass
    public static void shutdownDB() throws InterruptedException {
        mongo.close();
        mongoProcess.stop();
    }
    
    @Before
    public void setUp() throws Exception {
    	userRepository = new UserRepositoryImpl();
        template = new MongoTemplate(mongo, DB_NAME);
        userRepository.setMongoOps(template);
    }

    @After
    public void tearDown() throws Exception {
        template.dropCollection(Sample.class);
    }
    
    */
	
/*    
	@Test
	public void verifyExistingUser() {

	
		// given
		List<Action> actions = new ArrayList<Action>();
		Action action = new Action();
		action.setDate("08-12-2019");
		action.setWhat("teste");
		User user = new User();
		user.setCreationDate("08-12-2019");
		user.setName("Teste");
		user.setCrediCard("9999");
		
		
		// when
		userRepository.save(user);		
		
		// then
		 User user2 = userRepository.findByCrediCard("9999");
		 assertTrue(user2.getName().contains("Teste"));
		

	}
	
*/	

}
