package pt.augusto.cruz.gomes.group.audit.listener;



import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import pt.augusto.cruz.gomes.group.audit.documents.UserTransaction;
import pt.augusto.cruz.gomes.group.audit.domain.User;
import pt.augusto.cruz.gomes.group.audit.dts.UserConverter;
import pt.augusto.cruz.gomes.group.audit.mapper.ActionMapper;
import pt.augusto.cruz.gomes.group.audit.mongo.repository.UserRepository;
import pt.augusto.cruz.gomes.group.audit.repository.UserTransactionRepository;



@Component
public class Consumer {	

	@Autowired
	private UserTransactionRepository transactionRepository;
	
	@Autowired
	private UserRepository userRepository;

	@JmsListener(destination = "actions.queue")
	public void consume(String message) {

		ActionMapper mapper = new ActionMapper();

		UserTransaction obj = mapper.fromJsontoUserTransaction(message);

		System.out.println("Received from activemq: " + obj.toString());			
		
		UserConverter userConverter = new UserConverter();			
		
		User userDto = userConverter.convertUserTrancationToUpdateDB(obj);

		userRepository.save(userDto);

		List<User> users = userRepository.findAll();

		for (User u : users)
			System.out.println("User: " + u.getName());

	}

}
