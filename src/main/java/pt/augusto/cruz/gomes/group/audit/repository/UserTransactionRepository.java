package pt.augusto.cruz.gomes.group.audit.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import pt.augusto.cruz.gomes.group.audit.documents.UserTransaction;
import pt.augusto.cruz.gomes.group.audit.dto.UserDTO;



public interface UserTransactionRepository extends MongoRepository<UserTransaction, String> {

	void save(UserDTO userDto);

}
