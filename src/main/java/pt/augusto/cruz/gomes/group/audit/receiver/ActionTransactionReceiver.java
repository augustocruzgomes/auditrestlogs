package pt.augusto.cruz.gomes.group.audit.receiver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import pt.augusto.cruz.gomes.group.audit.documents.ActionTransaction;
import pt.augusto.cruz.gomes.group.audit.repository.ActionTransactionRepository;

@Component
public class ActionTransactionReceiver {

  @Autowired
  private ActionTransactionRepository transactionRepository;

  @JmsListener(destination = "OrderTransactionQueue", containerFactory = "myFactory")
  public void receiveMessage(ActionTransaction transaction) {
    System.out.println("Received <" + transaction + ">");
    transactionRepository.save(transaction);
  }
}