package pt.augusto.cruz.gomes.group.audit.dts;

import pt.augusto.cruz.gomes.group.audit.documents.UserTransaction;
import pt.augusto.cruz.gomes.group.audit.domain.User;

public class UserConverter {

	public User convertUserToUpdateDB(User userIn) {
		User userOut = new User();
		if(userIn.getUserid() != null)
			userOut.setUserid(userIn.getUserid());
		userOut.setName(userIn.getName());
		userOut.setCrediCard(userIn.getCrediCard());
		userOut.setPassword(userIn.getPassword());
		userOut.setToken(userIn.getToken());
		ActionsConverter actionsConverter = new ActionsConverter();
		userOut.setActions(actionsConverter.convertActionsToUpdateDB(userIn.getActions()));

		return userOut;

	}
	
	
	
	public User convertUserTrancationToUpdateDB(UserTransaction userIn) {
		User userOut = new User();
		if(userIn.getUserid() != null)
			userOut.setUserid(userIn.getUserid());
		userOut.setName(userIn.getName());
		userOut.setCrediCard(userIn.getCrediCard());
		userOut.setPassword(userIn.getPassword());
		userOut.setToken(userIn.getToken());
		userOut.setCreationDate(userIn.getCreationDate());
		ActionsConverter actionsConverter = new ActionsConverter();
		userOut.setActions(actionsConverter.convertActionTransactionsToUpdateDB(userIn.getActions()));
		userOut.setEmail(userIn.getEmail());

		return userOut;

	}
	

}
