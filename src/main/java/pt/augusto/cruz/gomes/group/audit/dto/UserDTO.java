package pt.augusto.cruz.gomes.group.audit.dto;

import java.util.List;

import pt.augusto.cruz.gomes.group.audit.documents.ActionTransaction;

public class UserDTO {
	
	private String userid;
	private String name;
	private String password;
	private String token;
	private String crediCard;
	private String creationDate;
	private List<ActionDTO> actions;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getCrediCard() {
		return crediCard;
	}
	public void setCrediCard(String crediCard) {
		this.crediCard = crediCard;
	}
	public List<ActionDTO> getActions() {
		return actions;
	}
	public void setActions(List<ActionDTO> actions) {
		this.actions = actions;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	
	
	

}
