package pt.augusto.cruz.gomes.group.audit.dts;

import java.util.ArrayList;
import java.util.List;

import pt.augusto.cruz.gomes.group.audit.documents.ActionTransaction;
import pt.augusto.cruz.gomes.group.audit.domain.Action;

public class ActionsConverter {
	
	

	public List<Action> convertActionsToUpdateDB(List<Action> actionsIn) {
		List<Action> actionsOut = new ArrayList<Action>();

		for (Action actionIn : actionsIn) {
			Action actionOut = new Action();
			if (actionIn.getId() != null)
				actionOut.setId(actionIn.getId());

			actionOut.setDate(actionIn.getDate());
			actionOut.setWhat(actionIn.getWhat());
			actionsOut.add(actionOut);

		}
		return actionsOut;
	}
	
	
	
	public List<Action> convertActionTransactionsToUpdateDB(List<ActionTransaction> actionsIn) {
		List<Action> actionsOut = new ArrayList<Action>();

		for (ActionTransaction actionIn : actionsIn) {
			Action actionOut = new Action();
			if (actionIn.getId() != null)
				actionOut.setId(actionIn.getId());

			actionOut.setDate(actionIn.getDate());
			actionOut.setWhat(actionIn.getWhat());
			actionsOut.add(actionOut);

		}
		return actionsOut;
	}


}
