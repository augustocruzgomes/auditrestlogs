package pt.augusto.cruz.gomes.group.audit.config;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;


/*
 * Necessario para converter string em LocalDate na requisição rest Post como JSON 
 * 
 * */
@ControllerAdvice
public class LocalDateTimeControllerAdvice {
	 @InitBinder
	    public void initBinder( WebDataBinder binder )
	    {
	        binder.registerCustomEditor( LocalDateTime.class, new PropertyEditorSupport()
	        {
	            @Override
	            public void setAsText( String text ) throws IllegalArgumentException
	            {
	                LocalDate.parse( text, DateTimeFormatter.ISO_DATE );
	            }
	        } );
	    }

}
