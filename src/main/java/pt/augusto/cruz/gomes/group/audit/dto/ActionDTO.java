package pt.augusto.cruz.gomes.group.audit.dto;

public class ActionDTO {
	private String 	id;
	private String date;
	private String what;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getWhat() {
		return what;
	}
	public void setWhat(String what) {
		this.what = what;
	}
		
	

}
