package pt.augusto.cruz.gomes.group.audit.mapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import pt.augusto.cruz.gomes.group.audit.documents.ActionTransaction;
import pt.augusto.cruz.gomes.group.audit.documents.UserTransaction;

public class ActionMapper {
	
	
	
	
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private  ObjectMapper objectMapper;

    public ActionMapper() {
    	objectMapper = new ObjectMapper();

    	//Registo de formato necesssário para converter LocalDate para Json e gravar na queue.
    	JavaTimeModule timeModule = new JavaTimeModule();
        timeModule.addSerializer(LocalDate.class, 
            new LocalDateSerializer(DateTimeFormatter.ISO_LOCAL_DATE));
        timeModule.addSerializer(LocalDateTime.class, 
            new LocalDateTimeSerializer(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        objectMapper.registerModule(timeModule);
    	
    }
    
    
    public String readJsonWithObjectMapper(UserTransaction  obj)  {
		 //ObjectMapper mapper = new ObjectMapper();
		String json = "";
		try {
			json = objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return json;
	}
    
    
    public String readJsonfromUserMapper(UserTransaction  obj)  {
		// ObjectMapper mapper = new ObjectMapper();
		String json = "";
		try {
			json = objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return json;
	}
    
    
    public ActionTransaction fromJsontoActionTransaction(String  json)  {
	   	// ObjectMapper mapper = new ObjectMapper();
	   	ActionTransaction objAction = null;
	   	
	   	
	   	try {
			objAction = objectMapper.readValue(json, ActionTransaction.class);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	   	
	   	return objAction;
   }
    
    public UserTransaction fromJsontoUserTransaction(String  json)  {
	   	// ObjectMapper mapper = new ObjectMapper();
	   	UserTransaction objAction = null;
	   	
	   	
	   	try {
			objAction = objectMapper.readValue(json, UserTransaction.class);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	   	
	   	return objAction;
  }
        
   

}
