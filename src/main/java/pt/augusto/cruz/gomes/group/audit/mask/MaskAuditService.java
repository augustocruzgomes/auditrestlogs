package pt.augusto.cruz.gomes.group.audit.mask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import pt.augusto.cruz.gomes.group.audit.domain.User;


@Service
public class MaskAuditService {
	
	@Autowired
	MaskCreditCard maskCreditCard;
	
	@Autowired
	MaskEmail maskEmail;
	
	

	public UserMasked mask(User user) {
		
		
		UserMasked userMasked = new UserMasked();
		
		userMasked.setUserid(user.getUserid());
		
		userMasked.setCreationDate(user.getCreationDate());
		userMasked.setName(user.getName());
		userMasked.setActions(user.getActions());
		userMasked.setCrediCard(maskCreditCard.mask(user.getCrediCard(), "#x-xxx##"));
		if(user.getEmail() != null)
			if (!user.getEmail().isEmpty())
				userMasked.setEmail(maskEmail.mask(user.getEmail(), ""));	
		
		
		
		return userMasked;
		
	}

}
