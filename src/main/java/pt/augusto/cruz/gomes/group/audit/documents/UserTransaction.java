package pt.augusto.cruz.gomes.group.audit.documents;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Document
@Getter
@Setter
@ToString(exclude = "userid")
@NoArgsConstructor
public class UserTransaction {
	
	@Id
	private String userid;
	private String name;
	private String password;
	private String token;
	private String crediCard;
//	@JsonSerialize(using = LocalDateTimeSerializer.class)
//	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
//	private LocalDateTime  creationDate;
//	@JsonFormat(pattern="yyyy-MM-dd") 
	@JsonFormat(pattern="yyyy-MM-dd") 
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate  creationDate;
	private List<ActionTransaction> actions;
	private String email;
	
	public UserTransaction(final String userid, final String name, final String password, final String token, final String crediCard, final LocalDate creationDate, final List<ActionTransaction> actions, final String email) {
		this.actions = actions;
		this.name = name;		
		this.password = password;
		this.token = token;
		this.crediCard = crediCard;
		this.creationDate = creationDate;
		this.email= email;
		
	}
	
	
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getCrediCard() {
		return crediCard;
	}
	public void setCrediCard(String crediCard) {
		this.crediCard = crediCard;
	}
	public List<ActionTransaction> getActions() {
		return actions;
	}
	public void setActions(List<ActionTransaction> actions) {
		this.actions = actions;
	}


	public LocalDate getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	








	


	


	


	
	
	
	

}
