package pt.augusto.cruz.gomes.group.audit.resources;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.jms.Queue;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.extern.slf4j.Slf4j;
import pt.augusto.cruz.gomes.group.audit.Exception.MyResourceNotFoundException;
import pt.augusto.cruz.gomes.group.audit.documents.ActionTransaction;
import pt.augusto.cruz.gomes.group.audit.documents.UserTransaction;
import pt.augusto.cruz.gomes.group.audit.domain.User;
import pt.augusto.cruz.gomes.group.audit.dts.UserConverter;
import pt.augusto.cruz.gomes.group.audit.mapper.ActionMapper;
import pt.augusto.cruz.gomes.group.audit.mask.MaskAuditService;
import pt.augusto.cruz.gomes.group.audit.mask.UserMasked;
import pt.augusto.cruz.gomes.group.audit.mongo.repository.UserRepository;
import pt.augusto.cruz.gomes.group.audit.repository.UserTransactionRepository;
@Slf4j
@RestController
@RequestMapping("/rest")
public class ActionsResources {

	@Autowired
	JmsTemplate jmsTemplate;

	@Autowired
	Queue queue;	

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserTransactionRepository userTransactionRepository;
	
	
	@Autowired
	MaskAuditService maskAuditService;

	@RequestMapping(value = "/actions", method = RequestMethod.GET)
	public String listar() {
		return "Rest aplicado, Ola from Rest";
	}

/*	
	@PostMapping("/send")
	public void send(@RequestBody ActionTransaction msg) {
		System.out.println("Sending a transaction.");
		ActionMapper mapper = new ActionMapper();
		String objJSON = mapper.readJsonWithObjectMapper(msg);
		jmsTemplate.convertAndSend(queue, objJSON);

		System.out.println("json ok.");

	}
*/
	
	@PostMapping("/sendUser")
	public void sendUser(@RequestBody UserTransaction msg) {
		System.out.println("Sending a user transaction.");
		ActionMapper mapper = new ActionMapper();
		User userInDB = null;
		boolean inserido = false;
		try {

			userInDB = userRepository.findByCrediCard(msg.getCrediCard());

			if (userInDB == null) {
				String objJSON = mapper.readJsonfromUserMapper(msg);
				jmsTemplate.convertAndSend(queue, objJSON);
				inserido = true;
				System.out.println("json ok.");
			} else {
				inserido = true;
				throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Utilizador ja existe", null);
			}

		} catch (IncorrectResultSizeDataAccessException e) {
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Utilizador Duplicado", null);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilizador não encontrado", e);
		} catch (RuntimeException e) {
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Utilizador ja existe", e);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Occoreu runtime exception", e);
		} finally {
			System.out.println("processo de criacao iniciado.");
			if (!inserido) {
				String objJSON = mapper.readJsonfromUserMapper(msg);
				jmsTemplate.convertAndSend(queue, objJSON);
				inserido = true;
				System.out.println("json ok.");
			}
		}

		throw new ResponseStatusException(HttpStatus.CREATED, "user inserted", null);

	}

	@SuppressWarnings("finally")
	@PutMapping("/users/{id}")
	public void replaceUser(@RequestBody UserTransaction userTransaction, @PathVariable String id) {
		System.out.println("Sending a user transaction.");

		Optional<User> userInDB = null;

		try {

			userInDB = userRepository.findById(id);

			UserConverter userConverter = new UserConverter();

			User userDto = userConverter.convertUserTrancationToUpdateDB(userTransaction);

			userRepository.deleteById(userInDB.get().getUserid());

			userRepository.save(userDto);
			String userID = userDto.getUserid();
			// get the updated object again
			Optional<User> userTest1_1 = userRepository.findById(userID);

			System.out.println("userTest1_1 - " + userTest1_1.get());

			System.out.println("update ok.");

		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilizador não encontrado", e);
		} catch (RuntimeException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilizador não encontrado", e);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilizador não encontrado", e);
		} finally {
			System.out.println("processo de udate iniciado.");
		}

		throw new ResponseStatusException(HttpStatus.ACCEPTED, "update ok", null);

	}

	@DeleteMapping(value = "/users/{id}")
	public ResponseEntity<Long> deletePost(@PathVariable String id) {

		boolean isRemoved = false;

		System.out.println("deleting  a user transaction.");

		Optional<User> userInDB = null;

		try {

			userInDB = userRepository.findById(id);

			if (userInDB.get() != null) {
				userRepository.deleteById(id);
				isRemoved = true;
			} else {
				isRemoved = false;
				throw new NoSuchElementException();
			}

		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilizador não encontrado", e);
		} catch (RuntimeException e) {
			if (!isRemoved) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping("/usersUnPaginated")
	public List<UserMasked> users() {
		List<UserMasked> maskedUsers = new ArrayList<UserMasked>();
		
		List<User> users = userRepository.findAll();
		
		for(User user : users ) {
			UserMasked userMasked = maskAuditService.mask(user);
			maskedUsers.add(userMasked);			
		}	
		
		return maskedUsers;

	}

	@GetMapping("/dateIntervals")
	public List<User> usersBYyDateIntervals(@RequestParam("DateIni") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate  DateIni,
			@RequestParam("EndDate")  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate  EndDate, Pageable pageable) {
		// @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) 
	//	@RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate
//		Criteria datesCriteria = Criteria.where("datesCriteria").gte(DateIni).lte(EndDate);
//		Query query = new Query(datesCriteria);

		// Date inputDate = new Date(DateIni);
		// TemporalAccessor date1 =
		// DateTimeFormatter.ofPattern("yyyy-MM-dd").parse("2017-12-31");
		// Instant inst1 = Instant.from(date1);

//		 LocalDate date1 = LocalDate.parse("31-12-2017");
//		 LocalDate date2 = LocalDate.parse("31-12-2019");
		//
		// Instant inst1 = date1.atStartOfDay(ZoneId.of("Europe/Paris")).toInstant();
		// Instant inst2 = date2.atStartOfDay(ZoneId.of("Europe/Paris")).toInstant();

		Instant inst1 = null, inst2 = null;
		try {
			inst1 = new SimpleDateFormat("yyyy-MM-dd").parse("2016-12-31").toInstant();
			inst2 = new SimpleDateFormat("yyyy-MM-dd").parse("2019-12-31").toInstant();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// TemporalAccessor date2 =
		// DateTimeFormatter.ofPattern("yyyy-MM-dd").parse("2020-12-31");
		// Instant inst2 = Instant.from(date2);

		Instant instant = Instant.parse("2019-10-01T08:25:24.00Z");

		String dt1 = "23-11-2017";
		String dt2 = "23-11-2020";

//		ZonedDateTime dataIni =  DateIni.atStartOfDay(ZoneId.systemDefault());
//		
//		ZonedDateTime dataFim = EndDate.atStartOfDay(ZoneId.systemDefault());
		
		
		// List<User> users = userRepository.findUsersByDateBetween(inst1, inst2);

		List<User> users = userRepository.findUsersByDateBetween(DateIni, EndDate);

		// Page<User> todos = userRepository.findAll(publishedDateCriteria, pageable);

		return users;

	}

	@GetMapping("/users/{userid}")
	public Optional<User> userid(@PathVariable("userid") final String userid) {
		Optional<User> user = userRepository.findById(userid);
		return user;
	}

	@GetMapping(params = { "page", "size" })
	public List<User> findPaginatedOrderedByName(@RequestParam("page") int page, @RequestParam("size") int size,
			UriComponentsBuilder uriBuilder, HttpServletResponse response) {

		Page<User> resultPage = null;
		try {

			Pageable pageable;

			Pageable sortedByName = PageRequest.of(page, size, Sort.by("name"));

			resultPage = userRepository.findAll(sortedByName);

			if (page > resultPage.getTotalPages()) {
				throw new MyResourceNotFoundException();
			}

		} catch (MyResourceNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilizadores não encontrados", e);

		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilizadore não encontrado", e);

		} catch (RuntimeException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilizadores não encontrados", e);
		}

		return resultPage.getContent();
	}
	
//	@PostMapping
//    public LocalDate post(@RequestBody PostBody body) {
//        log.info("Posted body: " + body);
//        return body.getDate().plus(10, ChronoUnit.YEARS);
//    }

}
