package pt.augusto.cruz.gomes.group.audit.mask;

import org.springframework.stereotype.Component;

@Component
public class MaskEmail implements MaskAttribute{

	@Override
	public String mask(String input, String maskPattern) {
		String maskedEmail = input.replaceAll("(?<=.{1}).(?=[^@]*?.@)", "*");
		return maskedEmail;
	}

}
