package pt.augusto.cruz.gomes.group.audit.mongo.repository;


import java.time.Instant;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import pt.augusto.cruz.gomes.group.audit.domain.User;

public interface UserRepository extends MongoRepository<User, String> {
	
	public User findByName(String name);
	public List<User> findUsersByCrediCard(String crediCard);
	public User findByCrediCard(String crediCard);
	
//	@Query("{ 'creationDate' : { $gt: ?0, $lt: ?1 } }")
//	List<User> findUsersByDateBetween(Instant  dateGT, Instant endDate);
	
	@Query("{ 'creationDate' : { $gt: ?0, $lt: ?1 } }")
	List<User> findUsersByDateBetween(LocalDate  dateGT, LocalDate endDate);

}
