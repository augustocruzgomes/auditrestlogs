package pt.augusto.cruz.gomes.group.audit.mask;

import org.springframework.stereotype.Component;

@Component
public class MaskCreditCard implements MaskAttribute{
	


	@Override
	public String mask(String input, String maskPattern) {
	
		// format the number
	    int index = 0;
	    StringBuilder maskedNumber = new StringBuilder();
	    for (int i = 0; i < maskPattern.length(); i++) {
	        char c = maskPattern.charAt(i);
	        if (c == '#') {
	            maskedNumber.append(input.charAt(index));
	            index++;
	        } else if (c == 'x') {
	            maskedNumber.append(c);
	            index++;
	        } else {
	            maskedNumber.append(c);
	        }
	    }
	    return maskedNumber.toString();
	}
	
	
	
}
