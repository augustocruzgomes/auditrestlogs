package pt.augusto.cruz.gomes.group.audit.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Document
@Getter
@Setter
@ToString(exclude = "id")
@NoArgsConstructor
public class ActionTransaction {
	@Id
	private String id;
	private String date;
	private String what;

	public ActionTransaction(final String id, final String date, final String what) {
		this.id = id;
		this.date = date;		
		this.what = what;
	}
	
	
	@Override
	public String toString() {
		String json = ""
				+"{"+		
				"	\"id\" : " + "\"" + id +  "\","+
				"	\"date\" : " + "\"" +  date  + "\"," +
				"	\"what\" : " + "\"" + what + "\"" +
				"}";
		
		return json;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getWhat() {
		return what;
	}


	public void setWhat(String what) {
		this.what = what;
	}

}
