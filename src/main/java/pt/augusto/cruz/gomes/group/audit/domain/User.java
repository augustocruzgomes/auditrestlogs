package pt.augusto.cruz.gomes.group.audit.domain;


import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class User {
	@Id
	private String userid;
	private String name;
	private String password;
	private String token;
	private String crediCard;
	private List<Action> actions;	

	@JsonFormat(pattern="yyyy-MM-dd") 	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate  creationDate;	
	private String email;
	
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getCrediCard() {
		return crediCard;
	}
	public void setCrediCard(String crediCard) {
		this.crediCard = crediCard;
	}
	
	
	
    public List<Action> getActions() {
		return actions;
	}
	public void setActions(List<Action> actions) {
		this.actions = actions;
	}
	
	
	@Override

    public String toString(){

        return getName() + ", "+getCrediCard();

    }
	public LocalDate getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	

	
    

}
