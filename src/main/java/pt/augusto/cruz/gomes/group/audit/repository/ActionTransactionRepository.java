package pt.augusto.cruz.gomes.group.audit.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import pt.augusto.cruz.gomes.group.audit.documents.ActionTransaction;



public interface ActionTransactionRepository extends MongoRepository<ActionTransaction, String> {}
